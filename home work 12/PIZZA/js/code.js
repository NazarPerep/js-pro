
const [...radioInputs] = document.querySelectorAll("label.radio"), //Створюємо масив із кнопок "радіо", щоб використати під час додавання події "клік".
    [...ingredients] = document.querySelectorAll(".draggable"); // Створюємо масив із інгредієнтів для використання під час додавання події "драг та дроп".

// Створюємо змінні для додавання та відображення ціни і назв вибраних інгредієнтів.
let priceSpan = document.getElementById("price-span"),
    sauceSpan = document.getElementById("sauce-span"),
    topingSpan = document.getElementById("toping-span"),
    cake = document.getElementById("cake");

// За допомогою ф-ї "forEach" додаїмо до кожного елемента масива подію "клік", при спрацюванні якої, додається і відображається ціна при виборі розміру піци.
radioInputs.forEach((element) => {
    element.addEventListener("click", (element) => {
        if (element.target.id == "small") {
            priceSpan.innerText = `50 грн`;
        } else if (element.target.id == "mid") {
            priceSpan.innerText = `75 грн`;
        } else if (element.target.id == "big") {
            priceSpan.innerText = `100 грн`;
        }
    })
});

// За допомогою ф-ї "forEach" на вибраному елементі масива викликаємо подію "dragstart" для переносу вибраного елемента.
ingredients.forEach((element) => {
    element.addEventListener("dragstart", (ev) => {
        ev.dataTransfer.setData("text", ev.target.id);
        ev.dataTransfer.effectAllowed = "copy";
    });
    // В кінці першої викликаної події перевіряємо, чи правильний елемент вибрано.
    element.addEventListener('dragend', (ev) => {
        console.log(ev);
    }, false);
});

// Викликаємо подію, коли переносний елемент знаходиться над областю перетягування.
cake.addEventListener("dragover", (e) => {
    if (e.preventDefault) e.preventDefault();
    return false;
});

// Переносний елемент опустився над цільовою областю.
cake.addEventListener("drop", (e) => {
    if (e.preventDefault) e.preventDefault();
    if (e.stopPropagation) e.stopPropagation();
    let id = e.dataTransfer.getData("text");
    let copyImg = document.createElement("img");
    let originImg = document.getElementById(id);
    copyImg.src = originImg.src;
    cake.appendChild(copyImg);

    // Під час опускання елементу спрацьовує умовний цикл для додавання назви вибраного інгредієнта і додавання ціни.
    switch (id) {
        case "sauceClassic":
            sauceSpan.innerText = "Кетчуп";
            if (priceSpan.innerText.length === 6) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 2)) + 20} грн`;
            } else if (priceSpan.innerText.length === 7) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 3)) + 20} грн`;
            } else {
                priceSpan.innerText = `20 грн`;
            }
            break;
        case "sauceBBQ":
            sauceSpan.innerText = "BBQ";
            if (priceSpan.innerText.length === 6) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 2)) + 30} грн`;
            } else if (priceSpan.innerText.length === 7) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 3)) + 30} грн`;
            } else {
                priceSpan.innerText = `30 грн`;
            }
            break;
        case "sauceRikotta":
            sauceSpan.innerText = "Рікотта";
            if (priceSpan.innerText.length === 6) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 2)) + 35} грн`;
            } else if (priceSpan.innerText.length === 7) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 3)) + 35} грн`;
            } else {
                priceSpan.innerText = `35 грн`;
            }
            break;
        case "moc1":
            topingSpan.innerText += " Сир звичайний,";
            if (priceSpan.innerText.length === 6) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 2)) + 40} грн`;
            } else if (priceSpan.innerText.length === 7) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 3)) + 40} грн`;
            } else {
                priceSpan.innerText = `20 грн`;
            }
            break;
        case "moc2":
            topingSpan.innerText += " Сир фета,";
            if (priceSpan.innerText.length === 6) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 2)) + 45} грн`;
            } else if (priceSpan.innerText.length === 7) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 3)) + 45} грн`;
            } else {
                priceSpan.innerText = `45 грн`;
            }
            break;
        case "moc3":
            topingSpan.innerText += " Моцарелла,";
            if (priceSpan.innerText.length === 6) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 2)) + 50} грн`;
            } else if (priceSpan.innerText.length === 7) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 3)) + 50} грн`;
            } else {
                priceSpan.innerText = `50 грн`;
            }
            break;
        case "telya":
            topingSpan.innerText += " Телятина,";
            if (priceSpan.innerText.length === 6) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 2)) + 70} грн`;
            } else if (priceSpan.innerText.length === 7) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 3)) + 70} грн`;
            } else {
                priceSpan.innerText = `70 грн`;
            }
            break;
        case "vetch1":
            topingSpan.innerText += " Помідори, ";
            if (priceSpan.innerText.length === 6) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 2)) + 20} грн`;
            } else if (priceSpan.innerText.length === 7) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 3)) + 20} грн`;
            } else {
                priceSpan.innerText = `20 грн`;
            }
            break;
        case "vetch2":
            topingSpan.innerText += " Гриби,";
            if (priceSpan.innerText.length === 6) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 2)) + 20} грн`;
            } else if (priceSpan.innerText.length === 7) {
                priceSpan.innerText = `${parseFloat(priceSpan.innerText.slice(0, 3)) + 20} грн`;
            } else {
                priceSpan.innerText = `20 грн`;
            }
            break;

        default: return;
    }
})

// Викликаємо на область наведення мишки подію для неможливості зайти в її межі.
const banner = document.getElementById("banner");
banner.addEventListener("mouseover", () => {
    banner.style.bottom = Math.floor(Math.random() * 80) + "%";
    banner.style.right = Math.floor(Math.random() * 80) + "%";
});

// Створюємо масив всіх інпутів.
const [...allInputs] = document.querySelectorAll("input");
localStorage.user = JSON.stringify([]); // створюємо місце для збереження інформації користувачів.

// Створюємо клас для нових користувачів
class User {
    constructor(name, phone, email) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.status = true;
    }
}

// Створюємо новий масив із інпутів, які будуть отримувати інформацію від користувача
let inputsRez = allInputs.map((e) => {
    return e;
}).filter((e) => {
    return e.type != "radio" && e.type != "reset" && e.type != "button";
});

// Створюємо функцію для валідації введених даних користувачем.
const validate = (target) => {
    switch (target.name) {
        case "name":
            return /^[А-я]{2,}$/i.test(target.value);
        case "phone":
            return /^\+380\d{9}$/.test(target.value);
        case "email":
            return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(target.value);
        default:
            throw new Error("Невірний виклик регулярного виразу!");
    }
}

inputsRez.forEach((e) => {
    e.addEventListener("change", (event) => {
        console.log(validate(event.target))
    })
})

let btnSub = document.querySelector("[type=button]");
// Додаємо подію при натисканні на кнопку підтвердження, яка перевіряж валідацію
btnSub.addEventListener("click", () => {
    let rez = inputsRez.map((e) => {
        return validate(e);
    });
// Створюємо умову, при якій, якщо інформація заповнена вірно, відсилається в LocalStorage
    if (!rez.includes(false)) {
        let a = JSON.parse(localStorage.user);
        a.push(new User(...inputsRez.map((e) => {
            return e.value
        })))
        localStorage.user = JSON.stringify(a);
        inputsRez.forEach((e) => {
            e.value = "";
        })
    }
});

// Створюємо подію для видалення інформації при натисканні на кнопку ресет.
let btnReset = document.querySelector("[type=reset]");
btnReset.addEventListener("click", () => {
    inputsRez.forEach((e) => {
        e.value = "";
    })
});