const STYLES = ["Джаз", "Блюз"];
document.write(STYLES + "<br>"); //створення масиву STYLES;

STYLES.push("Рок-н-ролл");
document.write("<br>" + STYLES + "<br>"); // добавлено в кінець масиву новий елемент "Рок-н-ролл";

const MID = (STYLES.length - 1) / 2;  // створюємо змінну MID та присвоюємо їй значення середнього елемента масиву;
STYLES.length % 2 != 0 ? STYLES.splice(MID, 1, "Класика") : alert("Неможливо знайти середнього елемента у масиві з парною сумою елементів!");
document.write("<p>" + STYLES + "<p>");

STYLES.unshift("Реп", "Реггі"); // добавлено елементи "Реп", "Реггі" на початок масиву;
document.write("<p>" + STYLES + "<p>");

// Class work!
document.write("<hr><hr><hr>")
//----1----
const LETT = ["a", "b", "c"], NUM = [1, 2, 3];
document.write("Перший масив LETT = [" + LETT + "]. <br>");
document.write("Другий масив NUM = [" + NUM + "]. <br>");
const CON = LETT.concat(NUM);
document.write("LETT + NUM = CON = [" + CON + "].");
document.write("<hr><hr>");

//----2----
const A = ["a", "b", "c"];
document.write("Масив А = [" + A + "]. <br>");
document.write("A.push(1,2,3). <br>");
A.push(1, 2, 3);
document.write("Масив А = [" + A + "].")
document.write("<hr><hr>");

//----3----
const B = [1, 2, 3];
document.write(" Масив B = [" + B + "]. <br>");
B.reverse();
document.write(" Масив B = [" + B + "]. <br>");
document.write("<hr><hr>");

//----4----
const C = [1, 2, 3];
document.write("Масив C = [" + C + "]. <br>");
document.write("C.push(4,5,6). <br>");
C.push(4, 5, 6);
document.write("Масив C = [" + C + "].")
document.write("<hr><hr>");

//----5----
const D = [1, 2, 3];
document.write("Масив D = [" + D + "]. <br>");
document.write("D.unshift(4,5,6). <br>");
D.unshift(4, 5, 6);
document.write("Масив D = [" + D + "].")
document.write("<hr><hr>");

//----6----
const FE = ['js', 'css', 'jq'];
document.write("Масив FE = [" + FE + "]. <br>");
document.write("Перший елемент масива FE = [" + FE[0] + "]. <br>");
document.write("<hr><hr>");

//----7----
const E = [1, 2, 3, 4, 5];
document.write("Масив Е = [" + E + "]. <br>");
const F = E.slice(0, 3);
document.write("Масив F = [" + F + "]. <br>");
document.write("<hr><hr>");

//----8----
const G = [1, 2, 3, 4, 5];
document.write("Масив G = [" + G + "]. <br>");
const g = G.splice(1, 2);
document.write("Елементи, які видалені = " + g + ". <br>");
document.write("Елементи, які залишились в масиві G = [" + G + "]. <br>");
document.write("<hr><hr>");

//----9----
const H = [1, 2, 3, 4, 5];
document.write("Масив Н = [" + H + "]. <br>");
H.splice(2, 0, 10);
document.write("Масив Н = [" + H + "]. <br>");
document.write("<hr><hr>");

//----10----
const I = [3, 4, 1, 2, 7];
document.write("Масив I = [" + I + "]. <br>");
I.sort();
document.write(I.join("-"));
document.write("<hr><hr>");

//----11----
const J = ["Привіт,", "світ", "!"];
document.write("<strong>" + J.join("") + "</strong>");
document.write("<hr><hr>");

//----12----
const K = ["Привіт,", "світ", "!" + "<br>"];
document.write(K.join(""));
K.splice(0, 1, "Бувай,");
document.write(K.join(""));
document.write("<hr><hr>");

//----13----
const firstMethod = [1, 2, 3, 4, 5];
document.write("firstMethod = [" + firstMethod + "]. <br>");
const secondMethod = new Array(1, 2, 3, 4, 5);
document.write("secondMethod = [" + secondMethod + "].");
document.write("<hr><hr>");

//----14----
const arr = {
    "ua": ["голубий", "червоний", "зелений"],
    "en": ["blue", "red", "green"],
};
document.write(arr.ua[0] + ", " + arr.en[0]);
document.write("<hr><hr>");

//----15----
const ARR = ["a", "b", "c", "d"];
document.write("'" + ARR[0] + "+" + ARR[1] + ", " + ARR[2] + "+" + ARR[3] + "'.");
document.write("<hr><hr>");

//----16----
const userArr = [];
userArr.length = (prompt("Введіть кількість елементів масиву:"));
for (let i = 0; i < userArr.length; i++) {
    document.write(i + "<br />");
};
document.write("<hr><hr>");

//----17----
const userArrSecond = [];
userArrSecond.length = (prompt("Введіть кількість елементів масиву:"));
for (let i = 0; i < userArrSecond.length; i++) {
    if (i % 2) {
        document.write("<p>" + i + "</p>");
    } else {
        document.write("<span style='background-color:red;'>" + i + "</span>");
    }
};
document.write("<hr><hr>");

//----18----
const vegetables = ["Капуста", "Ріпа", "Редиска", "Морква"];
let str1 = vegetables.join(", ");
document.write("'" + str1 + "'");












