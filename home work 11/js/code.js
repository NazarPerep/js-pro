// Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища
class User {
    constructor(name, lastname) {
        this.name = name;
        this.lastname = lastname;
    };
    showInfo() {
        console.log(`Hello ${this.name} ${this.lastname}!`);
    }
};

let ivan = new User("Ivan", "Mazur");
ivan.showInfo();

//Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 ервоний
const orderList = document.createElement("ol");
orderList.id = "order_list";
document.body.prepend(orderList);

const list1 = document.createElement("li");
list1.style.backgroundColor = "blue";

const list2 = document.createElement("li");

const list3 = document.createElement("li");
list3.style.backgroundColor = "red";

const list4 = document.createElement("li");

orderList.append(list1, list2, list3, list4);

//Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки
const div = document.createElement("div");
div.style.height = "400px";
div.style.width = "40%";
div.style.backgroundColor = "yellow";
div.style.margin = "0 auto";
div.style.display = "flex";
div.style.justifyContent = "center";
div.style.alignItems = "center";
orderList.after(div);
div.addEventListener("mousemove", (e) => {
    div.innerText = `X:${e.offsetX} / Y:${e.offsetY}`
});

// Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута
const divInputs = document.createElement("div");
divInputs.style.textAlign = "center";
divInputs.id = "container_inputs";
div.after(divInputs);
const input1 = document.createElement("input"),
    input2 = document.createElement("input"),
    input3 = document.createElement("input"),
    input4 = document.createElement("input");
divInputs.append(input1, input2, input3, input4);
input1.value = "1";
input1.type = "button";
input2.value = "2";
input2.type = "button";
input3.value = "3";
input3.type = "button";
input4.value = "4";
input4.type = "button";
let span = document.createElement("span");
span.id = "screen";
divInputs.append(span);

const print = (message) => {
    document.getElementById("screen").innerHTML += `${message}<br>`;
}
const clear = () => {
    document.getElementById("screen").innerHTML = "";
}

document.getElementById("main").addEventListener("keypress", (e) => {
    clear();
    print(`charCode = ${e.charCode}`);
    print(`code = ${e.code}`)
    print(`key = ${e.key}`)
});

const [...inputs] = document.getElementsByTagName("input");

inputs.forEach(function (e) {
    e.addEventListener("click", () => {
        alert(`You clicked number:${e.value}`)
    })
});

// Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці
let pHover = document.createElement("p");
divInputs.after(pHover);

pHover.addEventListener("mouseover", e => {
    e.target.classList = "big_hover";
});

pHover.addEventListener("mouseout", function () {
    this.classList.remove("big_hover");
})

// Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body
let inputColors = document.createElement("input");
inputColors.type = "color";
pHover.after(inputColors);

let divColor = document.createElement("div");
divColor.style.width = "100%";
divColor.style.height = "100px";
inputColors.after(divColor);

inputColors.addEventListener("click", () => {
    divColor.style.backgroundColor = inputColors.value;
})

// Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль
// Створіть поле для введення даних у полі введення даних виведіть текст під полем
let loginInput = document.createElement("input");
loginInput.placeholder = "Enter yours login";
loginInput.style.display = "inline";
divColor.after(loginInput);

let textDiv = document.createElement("div");
textDiv.style.display = "inline";
loginInput.after(textDiv);

loginInput.addEventListener("input", function () {
    console.log(loginInput.value);
    textDiv.innerText = `Login: ${loginInput.value}`;
});


