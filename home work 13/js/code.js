window.addEventListener("load", () => {

    class Hero {
        constructor(name, birth_year, gender, height, skin_color, homeworld) {
            this.name = name;
            this.birth_year = birth_year;
            this.gender = gender;
            this.height = height;
            this.skin_color = skin_color;
            this.homeworld = homeworld;
        };
    };

    let urlPeople = "https://swapi.dev/api/people";
    const heroesRequest = fetch(urlPeople, { method: "get" });
    let data1 = heroesRequest.then((res) => res.json(), (error) => console.error(error));

    data1.then((arg) => {
        let peopleArr = arg.results;
        showHeroes(peopleArr);
    });

    function showHeroes(array) {
        array.forEach(element => {
            const { name, birth_year, gender, height, skin_color, homeworld } = element;

            let heroCard = document.createElement("div");
            heroCard.classList = "hero-card";

            let nameH2 = document.createElement("h2");
            nameH2.innerText = `${element.name}`;

            let list = document.createElement("ul");
            list.innerHTML = `
            <li>Birth year : ${element.birth_year}</li>
            <li>Gender: ${element.gender}</li>  
            <li>Height: ${element.height}</li>  
            <li>Skin color: ${element.skin_color}</li> 
        `;

            let planetRequest = fetch(homeworld, { method: "get" });
            let planetData = planetRequest.then((responce) => responce.json(), (error) => console.error(error));
            planetData.then((planet) => {
                const { name } = planet;
                let liPlanet = document.createElement("li");
                liPlanet.innerText = `Homeworld: ${planet.name}`;
                list.append(liPlanet);

                inputSave.addEventListener("click", (input) => {
                    let planetRequest = fetch(homeworld, { method: "get" });
                    let planetData = planetRequest.then((responce) => responce.json(), (error) => console.error(error));
                    planetData.then((planet) => {
                        const { name } = planet;
                        if (input.target.id === element.height) {
                            let hero = new Hero(element.name, birth_year, gender, height, skin_color, planet.name);
                            alert(`You saved the hero: ${element.name} in Session storage!`);
                            console.log(hero);
                            sessionStorage.hero = `name: ${hero.name}, birth year: ${hero.birth_year}, gender: ${hero.gender}, height: ${hero.height}, skin color: ${hero.skin_color}, homeworld: ${hero.homeworld}`;
                            sessionStorage.hero = JSON.stringify(hero);
                        };
                    });
                });
            });

            let inputSave = document.createElement("input");
            inputSave.id = element.height;
            inputSave.type = "button";
            inputSave.classList = "btn_save";
            inputSave.value = "SAVE";

            heroCard.append(nameH2, list, inputSave);
            document.querySelector(".heroes-cards").append(heroCard);
        });
    };
});