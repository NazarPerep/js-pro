const header2 = document.createElement("h2");
header2.textContent = "Task №2 Number";
header2.id = "header_task2";
document.body.append(header2);

let leftPos = 0;
const animateSecond = () => {
    header2.style.left = leftPos + "px";
    leftPos += 1;
    if (leftPos > 900) {
        leftPos = 100;
    }
}
setInterval(animateSecond, 10);

const formNumber = document.createElement("form");
document.body.append(formNumber);

const labelNumber = document.createElement("label");
labelNumber.setAttribute("for", "get_number");
labelNumber.innerText = "Enter your number:";
formNumber.append(labelNumber);

const textArea = document.createElement("input");
textArea.classList.add("text_area");
textArea.type = "text";
textArea.id = "get_number";
textArea.placeholder = "000-000-00-00";
formNumber.append(textArea);

const inputNumberLine = document.createElement("div");
inputNumberLine.id = "input_line";
formNumber.append(inputNumberLine);

const submitBtn = document.createElement("input");
submitBtn.type = "button";
submitBtn.id = "submit_btn";
submitBtn.value = "SAVE";
inputNumberLine.append(submitBtn);

const resetBtnNumb = document.createElement("input");
resetBtnNumb.type = "reset";
resetBtnNumb.id = "reset_btn_number";
resetBtnNumb.value = "RESET";
inputNumberLine.append(resetBtnNumb);


submitBtn.onclick = () => {
    const pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;
    let enterNumber = textArea.value;
    if (pattern.test(enterNumber)) {
        document.location = "https://itc.ua/wp-content/uploads/2021/11/241176962_123109040071849_4424894243579652248_n.png";
        textArea.style.backgroundColor = "rgb(29, 193, 0)";
        console.log("ok");
    } else {
        textArea.value = "INVALID NUMBER";
        textArea.style.backgroundColor = "red";
        console.log("invalid number");
    }
}





