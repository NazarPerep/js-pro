/*
var x = 6;
var y = 14;
var z = 4;
var result = x += y - x++ * z; // 1)...++(Postfix Increment - precedence 16); 2)...*...(Multiplication - precedence 13); 3)… - …(Subtraction - precedence 12); 4)… += …(Assignment - precedence 2);
document.write(result + "<br>");

x = 6;
y = 14;
z = 4;
result = z = --x - y * 5;  // 1)-- …(Prefix Decrement - precedence 15); 2)...*...(Multiplication - precedence 13); 3)… - …(Subtraction - precedence 12); 4)… = …(Assignment - precedence 2);
document.write(result + "<br>");


x = 6;
y = 14;
z = 4;
result = y /= x + 5 % z;  // 1)...%...(Remainder - precedence 13); 2)...+...(Addition - precedence 12); 3).../=...(Division assignment - precedence 2);
document.write(result + "<br>"); 

x = 6;
y = 14;
z = 4;
result = z - x++ + y * 5; // 1)...++(Postfix Increment - precedence 16); 2)...*...(Multiplication - precedence 13); 3)… - …(Subtraction - precedence 12, Associativity:left-to-right); 4)...+...(Addition - precedence 12, Associativity:left-to-right);
document.write(result + "<br>");

x = 6;
y = 14;
z = 4;
result = x = y - x++ * z;
document.write(result + "<br>"); // 1)...++(Postfix Increment - precedence 16); 2)...*...(Multiplication - precedence 13); 3)… - …(Subtraction - precedence 12); 4)… = …(Assignment - precedence 2);
*/
var i = 1;
var msg = "";
while (i < 10) {
    msg += i + " x 3 = " + (i * 3) + "<br>";
    i++;
}
document.write(msg);