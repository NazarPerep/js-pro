import React from "react";
import ReactDOM from "react-dom";

const App = function () {
    return (
        <div>
            <Months></Months>
            <ul>
                <li>January</li>
                <li>February</li>
                <li>March</li>
                <li>April</li>
                <li>May</li>
                <li>June</li>
                <li>July</li>
                <li>August</li>
                <li>September</li>
                <li>October</li>
                <li>November</li>
                <li>December</li>
            </ul>

            <Days></Days>
            <ul>
                <li>Monday</li>
                <li>Tuesday</li>
                <li>Wednesday</li>
                <li>Thursday</li>
                <li>Friday</li>
                <li>Saturday</li>
                <li>Sunday</li>
            </ul>

            <Zodiac></Zodiac>
            <ul>
                <li>Capricorn</li>
                <li>Aquarius</li>
                <li>Pisces</li>
                <li>Aries</li>
                <li>Taurus</li>
                <li>Gemini</li>
                <li>Cancer</li>
                <li>Leo</li>
                <li>Virgo</li>
                <li>Libra</li>
                <li>Scorpio</li>
                <li>Sagittarius</li>
            </ul>
        </div>
    )
};

const Months = function () {
    return (
        <h2>Months of the year</h2>
    )
},

    Days = function () {
        return (
            <h2>Days of the week</h2>
        )
    },

    Zodiac = function () {
        return (
            <h2>The Zodiac Signs</h2>
        )
    };

ReactDOM.render(<App></App>, document.querySelector("#root"));