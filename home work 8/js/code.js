document.querySelector("input").onclick = function () {   //за допомогою створеною кнопки в html коді, створюємо при натиску поле для вводу тексту;
  let diamCircleBtn = document.createElement("input");  //присвоюємо змінну створенній формі для вводу;
  diamCircleBtn.className = "btn-diameter";             //створюємо клас;
  diamCircleBtn.type = "text";                          //вказуємо тип форми;
  diamCircleBtn.placeholder = "Enter the diameter of the circle(Max.value 25)";  //вказуємо попередній текст поля для вводу;
  document.body.append(diamCircleBtn);                  //вставляємо створений елемент за допомогою метода append;

  let drawBtn = document.createElement("input");
  drawBtn.className = "btn-draw";
  drawBtn.type = "button";
  drawBtn.value = "DRAW!";
  document.body.append(drawBtn);

  drawBtn.onclick = function () {                        //за натиском кнопки "DRAW!" викликаємо функцію, яка за допомогою цикла створює 100 div із присвоєнням властивостей;
    for (let index = 0; index < 100; index++) {
      let div = document.createElement("div");
      let diamCircle = diamCircleBtn.value;
      div.className = "circle";
      div.style.width = `${diamCircle}px`;
      div.style.height = `${diamCircle}px`;
      div.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 92%, 47%)`;
      div.style.boxShadow = `0px -11px 11px hsl(${Math.floor(Math.random() * 360)}, 92%, 47%)`;
      document.body.append(div);
    }
    const [...divs] = document.getElementsByTagName("div");  //отримуємо масив з елементів div;
    divs.forEach((element) => {                              //за допомогою методу forEach перебираємо кожний елемент масива і при кліку на вказаний елемент видаляємо його;
      element.onclick = function () {
        element.remove();
      };
    });
  };
};

