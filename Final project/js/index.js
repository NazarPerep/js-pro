let footerBg = document.querySelector(".footer__background");
let footerBox = document.querySelector(".footer__box");
let footerAccordion = document.querySelector(".accordion-footer")
const mediaQuery = window.matchMedia('(max-width:768px)');

if (mediaQuery.matches) {
   footerBg.appendChild(footerAccordion);
   footerBox.remove()
} else {
    footerAccordion.remove()
}

