//------------Прямокутник-----------------

for (let width = 0; width < 40; width++) {
  document.write("* ");
}

for (let leftHeight = 0; leftHeight < 10; leftHeight++) {
  document.write("<br>" + "*");
  for (let rightHeight = 0; rightHeight < 115; rightHeight++) {
    document.write("&nbsp;");
  }
  document.write("*");
}

document.write("<br>");

for (let width = 0; width < 40; width++) {
  document.write("* ");
}

document.write("<hr><hr>");
document.write("<br>");

//-----------------Прямокутний трикутник--------------------------

for (let i = 0; i < 15; i++) {
  for (let j = (15 - i); j < 15; j++) {
    document.write("&nbsp;" + "* ");
  }
  document.write("<br>");
}

document.write("<hr><hr>");
document.write("<br>");

//----------------------Рівносторонній трикутник-------------------------

const n = 10;
for (let i = 1; i <= n; i++) {
  for (let j = i; j < n; j++) {
    document.write("&nbsp;&nbsp;&nbsp;");
  }
  for (let j = 1; j <= 2 * i - 1; j++) {
    if (i == n || j == 1 || j == 2 * i - 1) {
      document.write("* ");
    } else {
      document.write("&nbsp;&nbsp;&nbsp;");
    }
  }
  document.write("<br>");
}

document.write("<hr><hr>");
document.write("<br>");

//----------------Ромб---------------------------

let numb = 11;
let str = "";
for (let i = 1; i <= numb; i++) {
  for (let j = 1; j <= numb - i; j++) {
    str += "&nbsp\n";
  }
  for (let k = 0; k < 2 * i - 1; k++) {
    str += "*";
  }
  str += "<br>";
}
for (let i = 1; i < numb; i++) {
  for (let j = 1; j <= i; j++) {
    str += "&nbsp\n";
  }
  for (let k = 0; k < 2 * (numb - i) - 1; k++) {
    str += "*";
  }
  str += "<br>";
}
document.write(str);


document.write("<hr><hr>");
document.write("<br>");

//-------перепис if з використанням "?"-----------
document.write("a + b < 4");
let a = parseFloat(prompt("Введіть перше число:"));
b = parseFloat(prompt("Введіть друге число:"));
a + b < 4 ? answer = "Мало" : answer = "Багато";
alert(answer);


document.write("<hr><hr>");
document.write("<br>");

//-------перепис if з використанням декількох "?"-----------
document.write("Василь чи Директор");
let answeR;
let login = prompt("Василь чи Директор");
login === "Василь" ? answeR = "Привіт" :
  login === "Директор" ? answeR = "Добрий день" :
    answeR = "Невірно!";
alert(answeR);


document.write("<hr><hr>");
document.write("<br>");

//-------------------- А < B ------------------------

let A = parseFloat(prompt("Вкажіть число А"));
B = parseFloat(prompt("Вкажіть число В, яке має бути більшим за число А"));
sum = 0;

for (let i = A; i < B; i++) {
  sum += i;
}
document.write("Сума чисел між " + A + " та " + B + " = " + sum);

document.write("<br>");

for (let j = B; j > A; j--) {
  if (j % 2 == 1) {
    document.write(j + ", ")
  }
}






