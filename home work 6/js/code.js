//-------створюємо масив із 5 елементів---------
const arrHumans = new Array(5);

//-------створюємо функцію-конструктор, яка буде створювати об'єкти Human----------
function Human(name, age, hobby) {
    this.name = name;
    this.age = age;
    this.hobby = hobby;
};

//------створюємо метод прототипа Human, який буде доступний кожному екземпляру------
Human.prototype.welcome = function () {
    alert(`Hi ${this.name}! Look, what we got!`)
}

//----створюємо цикл для наповнення масиву новими об'єктами---------
for (let i = 0; i < arrHumans.length; i++) {
    let quest = confirm(`Would you like to go in array :)?`) ? arrHumans[i] = new Human(prompt(`What is your name?`), prompt(`How old are you?`), prompt(`What is yours hobby?`)) : alert(`OK!`); {
        document.write(`Human №${[i + 1]} <br><br> Name: ${arrHumans[i].name}; <br> Age: ${arrHumans[i].age} years; <br> Hobby: ${arrHumans[i].hobby}. <br><hr><hr>`);
    }
}

//-------цикл для використання методу прототипа Human--------
for (let num = 0; num < arrHumans.length; num++) {
    arrHumans[num].welcome();
}


document.write(`Array has ${arrHumans.length} people. <br><hr><hr>`);

//-----------створюємо масив із значень об'єкта для можливості стортування----
let arrAge = [
    arrHumans[0]['age'],
    arrHumans[1]['age'],
    arrHumans[2]['age'],
    arrHumans[3]['age'],
    arrHumans[4]['age']
];

//-------створюємо функцію для правильного обчислення значень об'єкта------
let compareAge = function (a, b) {
    return a - b;
};

let reCompareAge = function (a, b) {
    return b - a;
};

//------разом із створеними функціями, застосовуємо метод .sort() та .join() для вивидення елементів масу із меншого до більшого і навпаки----
document.write(`Min. => ${(arrAge.sort(compareAge)).join(" => ")} Max. <br>`);
document.write(`Max. => ${(arrAge.sort(reCompareAge)).join(" => ")} Min.`);






