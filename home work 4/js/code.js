/*
Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает каждый 
элемент массива этой функцией, возвращая новый массив.
*/

document.getElementById("title").innerHTML = `<p> Task n.1 </p>`;
const arr = [4, 4, 9, 10, 45, 32];
function map(fn, arr) {
  let newArr = [];
  for (i = 0; i < arr.length; i++) {
    newArr[i] = fn(arr[i]);
  }
  return newArr;
}
function fn(x) {
  return x * 2;
}
document.getElementById("arr").innerHTML = `<p>First Array: ${arr} </p>`;
document.getElementById("arr2").innerHTML = `<p>New Array: ${map(fn, arr)} </p>`;

/*
Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18. В ином случае она задаёт вопрос confirm 
и возвращает его результат.
1	function checkAge(age) {
2	if (age > 18) {
3	return true;
4	} else {
5	return confirm('Родители разрешили?');
6	} }
*/

let age;
function checkAge(age) {
  return age > 18 ? true : alert("Батьки дозволили???");
};

document.write("<hr> <hr> <hr>") 