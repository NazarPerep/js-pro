import React from "react";
import data from "../data/data";
import Table from "../table/table";

function App() {
    return (
        <>
            <h1>Курс Валют</h1>
            <Table data={data}></Table>
        </>
    )
}

export default App;