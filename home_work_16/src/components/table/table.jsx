import React from "react";

function Table(props) {
    return (
        <table>
            <thead>
                <tr>
                    <th>Номер</th>
                    <th>Назва</th>
                    <th>Ціна</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        {props.data.map((element) => {
                            return <p>{element.r030}</p>
                        })}
                    </td>
                    <td>
                        {props.data.map((element) => {
                            return <p>{element.txt}</p>
                        })}
                    </td>
                    <td>
                        {props.data.map((element) => {
                            return <p>{element.rate}</p>
                        })}
                    </td>
                </tr>
            </tbody>
        </table>
    )
}

export default Table;